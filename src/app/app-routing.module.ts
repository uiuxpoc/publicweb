import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LandingPageComponent } from './AccountApp/LandingPage/landingPage.component';
import { SignupComponent } from './AccountApp/SignupPage/signup.component';
import { LoginComponent } from './AccountApp/LoginPage/login.component';
import { UserMasterComponent } from './AdminApp/UserMaster/usermaster.component';
import { ConfirmMobileComponent } from './AccountApp/ConfirmMobile/confirm-mobile.component';


//Dashboard & internal pages
import { DashboardComponent } from './DashboardApp/Dashboard/dashboard.component'





const routes: Routes = [
    { path: '', redirectTo: '/index', pathMatch: 'full' },
    { path: 'index', component: LandingPageComponent },
    { path: 'login', component: LoginComponent },
    { path: 'signup', component: SignupComponent },
    { path: 'confirmMobile', component: ConfirmMobileComponent },
    { path: 'userMaster', component: UserMasterComponent },
    { path: 'dashboard', component: DashboardComponent },


];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
